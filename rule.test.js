const { ESLint } = require("eslint");
const overrideConfig = { env: { jest: true } }

async function getErrors(fileToTest) {
    const cli = new ESLint({
        overrideConfigFile: 'eslintrc.json',
        overrideConfig,
    });

    return cli.lintFiles([fileToTest]);
}

describe('Validate ESLint configs', () => {
    it('Checks config', async () => {
        const results = await getErrors('example.js')
        expect(results[0].messages).toMatchSnapshot()
    });
});
