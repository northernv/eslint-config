> Linting for the Lightbox API layer

## Install

```
npm install -D @northernv/eslint-config
```

-or-

```
yarn add -d @northernv/eslint-config
```

## Configure

Add a `.eslintrc` file to your project
```
{
  "extends": ["@northernv"]
}
```

# Caveat

ESLint is a peerDependency, so you must install it as well
```
npm install -D eslint
```

## Development

* NOTE: Yes `yarn lint` reruns jest test. This is to pass the default CI/CD tests found in the pipeline https://gitlab.com/northernv/gitlab-ci-templates/-/blob/master/publish-npm.yml
* To lint, run `yarn locallint`
